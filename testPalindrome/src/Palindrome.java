
public class Palindrome {

	public static boolean isPalindrome(String word) {
		if (word.length() > 1) {
			StringBuffer palindrome = new StringBuffer(word.toLowerCase());
			palindrome.reverse();

			if ((word.toLowerCase()).equals(palindrome.toString())) {
				return true;
			}
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(Palindrome.isPalindrome("Deleveled"));
	}
}
