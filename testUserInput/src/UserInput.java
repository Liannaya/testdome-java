

public class UserInput {

	public static class TextInput {

		private StringBuffer str = new StringBuffer();
		
		public void add(char c) {			
			str.append(c);
		}

		public String getValue() {
			return str.toString();
		}
	}

	public static class NumericInput extends TextInput {

		@Override
		public void add(char c) {
			if (c >= '0' && c <= '9'){
				super.add(c);
			}
		}

	}

	public static void main(String[] args) {
		TextInput input = new NumericInput();
		input.add('1');
		input.add('a');
		input.add('0');
		System.out.println(input.getValue());
	}
}