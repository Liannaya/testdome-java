package testTwoSum;
import java.util.*;

public class TwoSum {
	public static int[] findTwoSum(int[] list, int sum) {
		//throw new UnsupportedOperationException("Waiting to be implemented.");
		Map<Integer, Integer> inMap = new HashMap<Integer, Integer>();
		
		for (int i = 0; i < list.length; i++) {
			int need = sum -  list[i];
			if(inMap.containsKey(need)) {
				return new int[] {i, inMap.get(need)};
			}
			inMap.put(list[i], i);
		}
		return null;
	}

	public static void main(String[] args) {
		int[] indices = findTwoSum(new int[] { 1, 3, 5, 7, 9 }, 12);
		System.out.println(indices[0] + " " + indices[1]);
	}
}